import Header from "./components/others/Header";
import Redirections from "./components/routes/Redirections.jsx";
const App = () => {
    return (
        <div className="app">
            <div>
                <Header />
            </div>
            <div className="body">
                <Redirections />
            </div>
        </div>
    );
};

export default App;
