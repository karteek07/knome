const InputField = ({
    id,
    name,
    label,
    type,
    oninput,
    register,
    errors,
    validation,
    icon,
    customCss,
}) => {
    return (
        <div className="component">
            <div className="label">
                <label id={"label_" + id} htmlFor={name}>
                    {label}
                </label>
            </div>
            <div className="input-field">
                <input
                    id={id}
                    type={type}
                    name={name}
                    onInput={oninput}
                    style={customCss}
                    {...register(name, validation)}
                    className="form-control"
                />
                {/* <i className={`fa ${icon}`} style={{position:'relative',color: 'black', bottom: '2rem',left: '21rem', cursor: 'pointer'}}></i> */}
                <div className="errormessage">
                    {errors && errors[name]?.type === "required" && (
                        <div className="error">
                            Please enter {errors[name]?.message}
                        </div>
                    )}
                    {errors && errors[name]?.type === "minLength" && (
                        <div className="error">
                            Please enter minimum {errors[name]?.message}
                        </div>
                    )}
                    {errors && errors[name]?.type === "maxLength" && (
                        <div className="error">
                            Please enter maximum {errors[name]?.message}
                        </div>
                    )}
                    {errors && errors[name]?.type === "pattern" && (
                        <div className="error">
                            Please enter a valid {errors[name]?.message}
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

// InputField.defaultProps = {
//   id :"" ,
//   name :"" ,
//   label :"" ,
//   type :"" ,
//   register :"" ,
//   errors :"" ,
//   validation :"" ,
// }

export default InputField;
