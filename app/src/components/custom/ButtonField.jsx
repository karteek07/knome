// import React from 'react'
import { useForm } from "react-hook-form";

const ButtonField = ({
  id,
  name,
  value,
  type,
  register,
  errors,
  validation,
  onClick,
  onHover,
  color,
}) => {
  return (
    <div className="field">
      <div className="button">
        <input
          type={type}
          id={id}
          name={name}
          value={value}
          onClick={onClick}
          onMouseOver={onHover}
          register={''}
          errors={errors}
          validation={validation}
          className={`btn btn-${color}`}
        />
      </div>
    </div>
  );
};

export default ButtonField;
