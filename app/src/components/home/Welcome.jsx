import React from 'react';
import { Navigate } from 'react-router-dom';
import path from '../../path';
import user from '../routes/user';

const Welcome = () => {
    if (user.isAuth()) {
        return <Navigate to={path.home} replace />;
    }
    return <div>Welcome</div>;
};

export default Welcome;
