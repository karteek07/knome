import React from 'react';
import InputField from '../custom/InputField';
import Title from '../others/Title';
import path from '../../path';
// import auth, { user } from "../routes/auth";
import { Navigate, useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import ButtonField from '../custom/ButtonField';
import axios from 'axios';
import user from '../routes/user';

const Add = () => {
    if (!user.isAuth()) {
        alert('Not logged in');
        return <Navigate to={path.login} replace />;
    }

    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();
    const navigate = useNavigate();

    const onSubmit = async ({
        fathername,
        mothername,
        dob,
        sex,
        add1,
        add2,
        country,
        state,
        district,
        city,
        pincode,
        aadharno,
        panno,
        contactno1,
        contactno2,
        emailadd1,
        emailadd2,
    }) => {
        const email = user.getEmail();
        console.log('🚀 ~ file: Add.jsx:45 ~ Add ~ email:', email);
        const check = await axios.post(path.api.add, {
            email,
            fathername,
            mothername,
            dob,
            sex,
            add1,
            add2,
            country,
            state,
            district,
            city,
            pincode,
            aadharno,
            panno,
            contactno1,
            contactno2,
            emailadd1,
            emailadd2,
        });
        if (check.data.message == 'success') {
            console.log('Hello there!');
        }
    };

    return (
        <div className='add'>
            <Title title='Your Data' />
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className='section'>
                    <div className='section-title'>Basic Details</div>
                    <div className='section-components-2'>
                        <InputField
                            id='fname'
                            name='fname'
                            label='First Name'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'first name',
                            }}
                        />
                        <InputField
                            id='lname'
                            name='lname'
                            label='Last Name'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'last name',
                            }}
                        />
                        <InputField
                            id='fathername'
                            name='fathername'
                            label="Father's Name"
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: "father's name",
                            }}
                        />
                        <InputField
                            id='mothername'
                            name='mothername'
                            label="Mother's Name"
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: "mother's name",
                            }}
                        />
                        <InputField
                            id='dob'
                            name='dob'
                            label='Date of Birth'
                            type='date'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'date of birth',
                            }}
                        />
                        <InputField
                            id='sex'
                            name='sex'
                            label='Sex'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'sex',
                            }}
                        />
                    </div>
                </div>
                <div className='section'>
                    <div className='section-title'>Address Details</div>
                    <div className='section-components-2'>
                        <InputField
                            id='add1'
                            name='add1'
                            label='Address 1'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'address 1',
                            }}
                        />
                        <InputField
                            id='add2'
                            name='add2'
                            label='Address 2'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'address 2',
                            }}
                        />
                        <InputField
                            id='country'
                            name='country'
                            label='Country'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'country',
                            }}
                        />
                        <InputField
                            id='state'
                            name='state'
                            label='State'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'state',
                            }}
                        />
                        <InputField
                            id='district'
                            name='district'
                            label='District'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'district',
                            }}
                        />
                        <InputField
                            id='city'
                            name='city'
                            label='City'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'city',
                            }}
                        />
                        <InputField
                            id='pincode'
                            name='pincode'
                            label='Pincode'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'pincode',
                            }}
                        />
                    </div>
                </div>
                <div className='section'>
                    <div className='section-title'>Identity Details</div>
                    <div className='section-components-2'>
                        <InputField
                            id='aadharno'
                            name='aadharno'
                            label='Aadhar No.'
                            type='text'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'aadhar no.',
                            }}
                        />
                        <InputField
                            id='panno'
                            name='panno'
                            label='Pan no.'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'uppercase' }}
                            validation={{
                                required: 'pan no.',
                            }}
                        />
                    </div>
                </div>
                <div className='section'>
                    <div className='section-title'>Contact Details</div>
                    <div className='section-components-2'>
                        <InputField
                            id='contactno1'
                            name='contactno1'
                            label='Contact 1'
                            type='text'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'contact 1',
                            }}
                        />
                        <InputField
                            id='contactno2'
                            name='contactno2'
                            label='Contact 2'
                            type='text'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'contact 2',
                            }}
                        />
                        <InputField
                            id='emailadd1'
                            name='emailadd1'
                            label='Email 1'
                            type='email'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'email 1',
                            }}
                        />
                        <InputField
                            id='emailadd2'
                            name='emailadd2'
                            label='Email 2'
                            type='email'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'email 2',
                            }}
                        />
                    </div>
                </div>
                <ButtonField
                    id='addSubmit'
                    name='addSubmit'
                    type='submit'
                    value='Submit'
                    color='primary'
                />
            </form>
        </div>
    );
};

export default Add;
