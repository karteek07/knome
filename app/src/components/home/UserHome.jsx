import React from 'react';
import { Link, Navigate } from 'react-router-dom';
import path from '../../path';
import user from '../routes/user';
import Title from '../others/Title';
import Add from './Add';

const UserHome = () => {
    if (!user.isAuth()) {
        return <Navigate to={path.welcome} replace />;
    }
    return (
        <div className='userhome'>
            <div className='body'>
                <div className='view'></div>
                <div className='add'>
                    <Link to='/add'>Add</Link>
                </div>
            </div>
        </div>
    );
};

export default UserHome;
