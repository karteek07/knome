import React from 'react';
import ButtonField from '../custom/ButtonField';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import path from '../../path';
import user from '../routes/user';

const Header = () => {
    const navigate = useNavigate();
    const location = useLocation();
    // console.log("Hello");
    return (
        <div className='header'>
            {user.isAuth() ? (
                <Link to={path.home}>
                    <div className='logo'>K N O M E</div>
                </Link>
            ) : (
                <Link to={path.welcome}>
                    <div className='logo'>K N O M E</div>
                </Link>
            )}
            {/* <div className="categories">Categories</div> */}
            <div className='loginLogout'>
                {user.isAuth() ? (
                    <div className='logout'>
                        <ButtonField
                            id='logout'
                            name='logout'
                            type='button'
                            value='Logout'
                            color='danger'
                            onClick={() => {
                                user.deauth();
                                user.removeEmail();
                                navigate(path.welcome);
                            }}
                        />
                    </div>
                ) : location.pathname == path.login ||
                  location.pathname == path.register ? (
                    <></>
                ) : (
                    <div className='login'>
                        <ButtonField
                            id='login'
                            name='login'
                            type='button'
                            value='Login'
                            color='primary'
                            onClick={() => {
                                navigate(path.login);
                            }}
                        />
                    </div>
                )}
            </div>
        </div>
    );
};

export default Header;
