import React from 'react';

import notfound from './../../img/notfound.png';

class NotFound extends React.Component {
  render() {
    return (
      <div className='container'>
        <div className='d-flex text-center row justify-content-center'>
          <div>
            <div className='circled'>
              <img src={notfound} className="img-fluid img-responsive thumbnail" alt="Error 404 。。。Not Found" />
            </div>
            <div>
              <h3>
                &nbsp;&nbsp;。。。 Error Page not found&nbsp;&nbsp;&nbsp;。。。
              </h3>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NotFound;