import React from 'react';
import { Link } from 'react-router-dom';

export const StudentLoginFirst = () => {
  return (
    <div className='container'>
      <div className='row d-flex text-center justify-content-center'>
        <table className='table table-dark table-hover'>
          <tbody>
            <tr>
              <td>
                <h3>
                  <small>You've to</small>
                <div className='font-weight-bold'>
                    Login First as a Student
                </div>
                  <small>to Continue</small>
                </h3>
              </td>
            </tr>
            <tr>
              <td>
                <Link to='/student/login' className='font-weight-bold mark lead'>Student Login</Link>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export const TeacherLoginFirst = () => {
  return (
    <div className='container'>
      <div className='row d-flex text-center justify-content-center'>
        <table className='table table-dark table-hover'>
          <tbody>
            <tr>
              <td>
              <h3>
                  <small>You've to</small>
                <div className='font-weight-bold'>
                    Login First as a Teacher
                </div>
                  <small>to Continue</small>
                </h3>
              </td>
            </tr>
            <tr>
              <td>
                <Link to='/teacher/login' className='font-weight-bold mark lead'>Teacher Login</Link>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}