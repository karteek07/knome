import React from "react";
import { Routes, Route } from "react-router-dom";

import UserHome from "../home/UserHome";
import AdminHome from "../home/AdminHome";
import SignIn from "../auth/SignIn";
import SignUp from "../auth/SignUp";
import NotFound from "../others/NotFound";
import { ProtectedRoute } from "./ProtectedRoutes";
import Protected from "./Protected";
import auth from "./auth";
import Welcome from "../home/Welcome";

const Redirections = () => {
    return (
        <Routes>
            <Route path="/" element={<UserHome />} />
            <Route path="/welcome" element={<Welcome />} />
            <Route path="/signin" element={<SignIn />} />
            <Route path="/signup" element={<SignUp />} />
            <Route path="*" element={<NotFound />} />
            {/* <Route
                path="/admin"
                element={
                    <Protected isLoggedIn={auth.isUserAuthenticated()}>
                        <AdminHome />
                    </Protected>
                }
            /> */}
        </Routes>
    );
};

export default Redirections;
