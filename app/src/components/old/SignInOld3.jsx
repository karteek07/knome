import React from 'react'

const SignIn = () => {
    return (
        <div>
            <div className="title">
                Sign-In
            </div>
            <div className="body">
                <div className="section">
                    <div className="field">
                        <div className="label">
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="input">
                            <input type="email" name="email" id="email" />
                        </div>
                    </div>
                    <div className="field">
                        <div className="label">
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="input">
                            <input type="password" name="password" id="password" />
                        </div>
                    </div>
                    <div className="field">
                        <div className="button">
                            <input type="button" value="SIGN-IN" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SignIn