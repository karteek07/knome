import React from 'react'

const SignUp = () => {
    return (
        <div>
            <div className="title">
                Register
            </div>
            <div className="body">
                <div className="section">
                    <div className="field">
                        <div className="label">
                            <label htmlFor="email">Email</label>
                        </div>
                        <div className="input">
                            <input type="email" name="email" id="email" />
                        </div>
                    </div>
                    <div className="field">
                        <div className="label">
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="input">
                            <input type="password" name="password" id="password" />
                        </div>
                    </div>
                    <div className="field">
                        <div className="label">
                            <label htmlFor="cnfpassword">Confirm Password</label>
                        </div>
                        <div className="input">
                            <input type="password" name="cnfpassword" id="cnfpassword" />
                        </div>
                    </div>
                    <div className="field">
                        <div className="button">
                            <input type="button" value="SIGN-UP" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SignUp