import { useForm } from "react-hook-form";

// import React from 'react'

const SignIn = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm();
  return (
    <div>
      <div className="header">
        <div className="title">Sign In</div>
      </div>
      <div className="body">
        <div className="form">
          <div className="field">
            <div className="label">
              <label htmlFor="email">Email</label>
            </div>
            <div className="input">
              <input type="email" name="email" {...register("email")} />
            </div>
          </div>
          <div className="field">
            <div className="label">
              <label htmlFor="password">Password</label>
            </div>
            <div className="input">
              <input type="password" name="password" {...register("password")} />
            </div>
          </div>
          <div className="field">
            <div className="button">
              <input type="submit" value="Submit" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
