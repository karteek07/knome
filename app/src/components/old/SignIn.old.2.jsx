import { useForm } from "react-hook-form";
import InputField from "../custom/InputField";
import ButtonField from "../custom/ButtonField";

// import React from 'react'

const SignIn = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => console.log(data);

  return (
    <div>
      <div className="header">
        <div className="title">Sign In</div>
      </div>
      <div className="body">
        <div className="form">
          <form onSubmit={handleSubmit(onSubmit)}>
            <InputField
              label="Email"
              id="email"
              name="email"
              type="email"
              register={register}
              errors={errors}
              validation={{
                required: "Email",
                minLength: {
                  value: 3,
                  message: "Please enter a minimum of 3 characters",
                },
              }}
              required
            />
            <InputField
              label="Password"
              id="password"
              name="password"
              type="password"
              register={register}
              errors={errors}
              validation={{
                required: "Password",
                minLength: {
                  value: 3,
                  message: "Please enter a minimum of 3 characters",
                },
              }}
              required
            />
            <ButtonField
              id="signinSubmit"
              name="signinSubmit"
              type="submit"
              value="Submit"
              // onHover={()=>alert("Hello")}
              // onClick={onClick}
              // register={register}
              // errors={errors}
            />
          </form>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
