import { useLocation, useNavigate } from "react-router-dom";
import ButtonField from "./components/custom/ButtonField";
import Header from "./components/others/Header";
import Redirections from "./components/routes/Redirections";
import auth from "./components/routes/auth";
import { path } from "./paths";

const App = () => {
    const location = useLocation();
    const navigate = useNavigate();
    console.log(path.addr)
    return (
        <div>
            {/* <Header /> */}
            <div className="header">
                <div className="logo">Know Me</div>
                <div className="categories">Categories</div>
                <div className="loginLogout">
                    {auth.isUserAuthenticated() ? (
                        <div className="logout">
                            <ButtonField
                                id="logout"
                                name="logout"
                                type="button"
                                value="Logout"
                                onClick={() => {
                                    auth.makeUserDeauthenticate();
                                    navigate(path.welcome);
                                }}
                            />
                        </div>
                    ) : location.pathname == path.login ||
                      location.pathname == path.register ? (
                        <></>
                    ) : (
                        <div className="login">
                            <ButtonField
                                id="login"
                                name="login"
                                type="button"
                                value="Login"
                                onClick={() => {
                                    navigate(path.login);
                                }}
                            />
                        </div>
                    )}
                </div>
            </div>
            <Redirections />
        </div>
    );
};

export default App;
