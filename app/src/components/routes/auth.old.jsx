const auth = {
    isUserAuthenticated: () =>
        JSON.parse(sessionStorage.getItem("isUserAuthenticated")) === "true"
            ? true
            : false,
    makeUserAuthenticate: () =>
        sessionStorage.setItem("isUserAuthenticated", JSON.stringify("true")),
    makeUserDeauthenticate: () =>
        sessionStorage.setItem("isUserAuthenticated", JSON.stringify("false")),
};

export const user = {
    isAuth: () =>
        JSON.parse(sessionStorage.getItem("isAuth")) === "true" ? true : false,
    auth: () => sessionStorage.setItem("isAuth", JSON.stringify("true")),
    deauth: () => sessionStorage.setItem("isAuth", JSON.stringify("false")),
    getEmail: () => sessionStorage.getItem("user_email"),
    setEmail: (email) => sessionStorage.setItem("user_email", email),
    removeEmail: () => sessionStorage.removeItem("user_email"),
    getData: () => sessionStorage.getItem("user_data"),
    setData: (data) => sessionStorage.setItem("user_data", data),
    removeData: () => sessionStorage.removeItem("user_data"),
};

export default auth;
