import React from 'react';
import { Routes, Route } from 'react-router-dom';

import UserHome from '../home/UserHome';
import Login from '../auth/Login';
import Register from '../auth/Register';
import NotFound from '../others/NotFound';
import Welcome from '../home/Welcome';
import Add from '../home/Add';
import path from '../../path';

const Redirections = () => {
    return (
        <Routes>
            <Route path={path.home} element={<UserHome />} />
            <Route path={path.add} element={<Add />} />
            <Route path={path.welcome} element={<Welcome />} />
            <Route path={path.login} element={<Login />} />
            <Route path={path.register} element={<Register />} />
            <Route path='*' element={<NotFound />} />
        </Routes>
    );
};

export default Redirections;
