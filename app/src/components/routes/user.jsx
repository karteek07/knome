const user = {
    isAuth: () => JSON.parse(sessionStorage.getItem('isAuth')),
    auth: () => sessionStorage.setItem('isAuth', true),
    deauth: () => sessionStorage.setItem('isAuth', false),
    getEmail: () => sessionStorage.getItem('user_email'),
    setEmail: (email) => sessionStorage.setItem('user_email', email),
    removeEmail: () => sessionStorage.removeItem('user_email'),
    getData: () => sessionStorage.getItem('user_data'),
    setData: (data) => sessionStorage.setItem('user_data', data),
    removeData: () => sessionStorage.removeItem('user_data'),
};

export default user;
