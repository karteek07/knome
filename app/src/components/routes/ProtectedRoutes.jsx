import React from "react";
import { Route, Navigate } from "react-router-dom";

import auth from "./auth";

export const ProtectedRoute = ({ path, element: Element, ...rest }) => {
    return (
        <Route
            {...rest}
            render={(props) => {
                if (auth.isUserAuthenticated()) {
                    return <Element {...props} />;
                } else {
                    return <Navigate to={"/signin"} replace />;
                }
            }}
        />
    );

    // return;
};

// export const ProtectedRoute = ({ element: Element, ...rest }) => {
//     return (
//         <Route
//             {...rest}
//             render={(props) => {
//                 if (auth.isUserAuthenticated()) {
//                     return <Element {...props} />;
//                 } else {
//                     return <Navigate to={"/signin"} replace />;
//                 }
//             }}
//         />
//     );
// };
