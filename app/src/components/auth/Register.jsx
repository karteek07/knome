import React from 'react';
import { useForm } from 'react-hook-form';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import Title from '../others/Title';
import InputField from '../custom/InputField';
import ButtonField from '../custom/ButtonField';
import axios from 'axios';
import path from '../../path';
import user from '../routes/user';

const Register = () => {
    if (user.isAuth()) {
        return <Navigate to={path.home} replace />;
    }
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();
    const navigate = useNavigate();

    const onSubmit = async ({ fname, lname, email, password }) => {
        console.log(fname, lname, email, password);
        const alreadyRegister = await axios.post(path.api.alreadyRegistered, {
            email: email,
        });

        if (alreadyRegister.data.message == 'success') {
            alert('You are already registered, kindly login');
            return;
        }

        const check = await axios.post(path.api.register, {
            fname: fname,
            lname: lname,
            email: email,
            password: password,
        });

        if (check.data.message == 'success') {
            alert('Registered');
            user.setEmail(email);
            user.auth();
            navigate(path.home);
        }
    };
    return (
        <div>
            <div className='register-form'>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className='register-section-title'>Register</div>
                    <div className='register-section'>
                        <InputField
                            id='fname'
                            name='fname'
                            label='First Name'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'first name',
                            }}
                        />
                        <InputField
                            id='lname'
                            name='lname'
                            label='Last Name'
                            type='text'
                            register={register}
                            errors={errors}
                            customCss={{ textTransform: 'capitalize' }}
                            validation={{
                                required: 'last name',
                            }}
                        />

                        <InputField
                            id='email'
                            name='email'
                            label='Email'
                            type='email'
                            icon='fa-user'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'Email',
                                pattern: {
                                    value: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
                                    message: 'Email',
                                },
                            }}
                        />
                        <InputField
                            id='password'
                            name='password'
                            label='Password'
                            type='password'
                            icon='fa-key'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'Password',
                                pattern: {
                                    value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/,
                                    message:
                                        'password, minimum length is 5 & must have atleast one uppercase, one lowercase, one digit & one special character',
                                },
                            }}
                        />
                        <InputField
                            id='cnfpassword'
                            name='cnfpassword'
                            label='Confirm Password'
                            type='password'
                            icon='fa-lock'
                            register={register}
                            errors={errors}
                            validation={{
                                required: 'Confirm Password',
                            }}
                        />
                    </div>
                    <p>
                        Have an account,{' '}
                        <Link to={path.login}>&nbsp;login</Link>
                    </p>
                    <ButtonField
                        id='signinSubmit'
                        name='signinSubmit'
                        type='submit'
                        value='Register'
                        color='primary'
                    />
                </form>
            </div>
        </div>
    );
};

export default Register;
