import React from "react";
import axios from "axios";
import { useForm } from "react-hook-form";
import InputField from "../custom/InputField";
import ButtonField from "../custom/ButtonField";
import { Link, Navigate, useNavigate } from "react-router-dom";
import path from "../../path";
import user from "../routes/user";

const Login = () => {
    if (user.isAuth()) {
        return <Navigate to={path.home} replace />;
    }
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm();

    const navigate = useNavigate();

    const onSubmit = async ({ email, password }) => {
        const check = await axios.post(path.api.login, {
            email: email,
            password: password,
        });
        if(check.data.message=='success'){
            user.setEmail(email)
            user.auth()
            navigate(path.home);
        } else {
            alert("Email or password is incorrect");
        }
    };
    return (
        <div>
            <div className="login-form">
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="login-section-title">Login</div>
                    <div className="login-section">
                        <InputField
                            id="email"
                            name="email"
                            label="Email"
                            type="email"
                            register={register}
                            errors={errors}
                            validation={{
                                required: "Email",
                                pattern: {
                                    value: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
                                    message: "Email",
                                },
                            }}
                        />
                        {/* <i className={`fa fa-user`} style={{position:'relative',color: 'black', top: '-4.7rem',left: '36.5rem', cursor: 'pointer'}}></i> */}
                        <InputField
                            id="password"
                            name="password"
                            label="Password"
                            type="password"
                            register={register}
                            errors={errors}
                            validation={{
                                required: "Password",
                                pattern: {
                                    value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,}$/,
                                    message:
                                        "password, minimum length is 5 & must have atleast one uppercase, one lowercase, one digit & one special character",
                                },
                            }}
                        />
                    </div>
                    <p>
                        Don't have an account,
                        <Link to={path.register}>&nbsp;register</Link>
                    </p>
                    <ButtonField
                        id="signinSubmit"
                        name="signinSubmit"
                        type="submit"
                        value="Login"
                        color="primary"
                    />
                </form>
            </div>
        </div>
    );
};

export default Login;
