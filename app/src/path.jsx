const appIp = 'http://127.0.0.1';
const appPort = '5173';
const serverIp = 'http://127.0.0.1';
// const serverIp = "http://192.168.100.21";
const serverPort = '3000';
const app = `${appIp}:${appPort}`;
// const server = `${serverIp}:${serverPort}`;
const server = 'https://knomeapi.vercel.app';
const path = {
    register: '/register',
    login: '/login',
    logout: '/logout',
    welcome: '/welcome',
    home: '/',
    add: '/add',
    app: app,
    server: server,
    api: {
        login: `${server}/login`,
        logout: `${server}/logout`,
        register: `${server}/register`,
        add: `${server}/add`,
        alreadyRegistered: `${server}/alreadyRegistered`,
    },
};

export default path;
