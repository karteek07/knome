const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
    fname: {
        type: String,
        trim: true,
        lowercase: true,
    },
    lname: {
        type: String,
        trim: true,
        lowercase: true,
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
    },
    password: {
        type: String,
        trim: true,
    },
    basic: {
        fathername: {
            type: String,
            trim: true,
        },
        mothername: {
            type: String,
            trim: true,
        },
        dob: {
            type: String,
            trim: true,
        },
        sex: {
            type: String,
            trim: true,
        },
    },
    address: {
        add1: {
            type: String,
            trim: true,
        },
        add2: {
            type: String,
            trim: true,
        },
        country: {
            type: String,
            trim: true,
        },

        state: {
            type: String,
            trim: true,
        },
        district: {
            type: String,
            trim: true,
        },
        city: {
            type: String,
            trim: true,
        },
        pincode: {
            type: String,
            trim: true,
        },
    },
    contact: {
        contactno1: {
            type: String,
            trim: true,
        },
        contactno2: {
            type: String,
            trim: true,
        },
        emailadd1: {
            type: String,
            trim: true,
        },
        emailadd2: {
            type: String,
            trim: true,
        },
    },
    identity: {
        aadharno: {
            type: String,
            trim: true,
        },
        panno: {
            type: String,
            trim: true,
        },
    },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
