const express = require("express");
// const process = require("process");
const cors = require("cors");
const bcrypt = require("bcrypt");
require("./db/mongoosedb");
const User = require("./models/User");

const PORT = 3000;
// const PORT = process.env.PORT;
const app = express();

app.use(cors());
app.use(express.json());

app.get("/", (req, res) => {
    res.send("Hii");
});

app.post("/check", async (req, res) => {
    const { email } = req.body;
    const user = await User.findOne({ email: email });

    return res.json({
        message: `${email}: the email you sent`,
        data: user.data,
    });
});

app.post("/alreadyRegistered", async (req, res) => {
    const { email } = req.body;
    const user = await User.findOne({ email: email });
    if (user) {
        return res.json({ message: "success" });
    }
    return res.json({ message: "failed" });
});

app.post("/register", async (req, res) => {
    const { fname, lname, email, password } = req.body;
    console.log("🚀 ~ file: server.js:29 ~ app.post ~ req.body:", req.body);
    const hashedPassword = await bcrypt.hash(password, 10);
    const userData = await new User({
        fname: fname,
        lname: lname,
        email: email,
        password: hashedPassword,
    });
    userData.save();
    return res.json({ message: "success" });
});

app.post("/login", async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({ email: email });
    if (!user) {
        return res.json({ message: `No user registered with email: ${email}` });
    }
    const hashedPassword = user.password;
    const passwordMatch = await bcrypt.compare(password, hashedPassword);
    if (passwordMatch) {
        return res.json({ message: "success" });
    } else {
        return res.json({ message: "failure" });
    }
});

app.post("/add", async (req, res) => {
    const {
        email,
        fathername,
        mothername,
        dob,
        sex,
        add1,
        add2,
        country,
        state,
        district,
        city,
        pincode,
        aadharno,
        panno,
        contactno1,
        contactno2,
        emailadd1,
        emailadd2,
    } = req.body;
    console.log("🚀 ~ file: server.js:76 ~ app.post ~ req.body:", req.body);
    const add = await User.findOneAndUpdate(
        { email: email },
        {
            $set: {
                basic: {
                    fathername: fathername,
                    mothername: mothername,
                    dob: dob,
                    sex: sex,
                },
                address: {
                    add1: add1,
                    add2: add2,
                    country: country,
                    state: state,
                    district: district,
                    city: city,
                    pincode: pincode,
                },
                identity: {
                    aadharno: aadharno,
                    panno: panno,
                },
                contact: {
                    contactno1: contactno1,
                    contactno2: contactno2,
                    emailadd1: emailadd1,
                    emailadd2: emailadd2,
                },
            },
        }
    );

    console.log(add);

    return res.json({ message: "success" });
});

app.listen(PORT, () => {
    console.log(`Server is running on port http://localhost:${PORT}`);
});

module.exports = app;
